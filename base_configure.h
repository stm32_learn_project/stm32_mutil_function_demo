#ifndef _BASE_CONFIGURE_H_
#define _BASE_CONFIGURE_H_
#endif

#include "stm32f10x.h"                  // Device header
#include "stm32f10x_tim.h"              // Keil::Device:StdPeriph Drivers:TIM


void OLED_GPIO_Init(void);
void LED_GPIO_Init(void);
void KEY_GPIO_Init(void);
void OS_Cell_Run_Timer_Config(void);
void Delay_Timer_Config(void);
void KEY_EXTI_Init(void);
void Delay_Timer_NVIC_Config(void);
void Init_configure(void);
