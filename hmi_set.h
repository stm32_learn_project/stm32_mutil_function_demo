#ifndef _HMI_SET_H_
#define _HMI_SET_H_

#include "hmi_common.h"

extern const MenuImage_t sgc_SettingImage;

void Hmi_SetEnter(void);
void Hmi_SetExit(void);
void Hmi_SetLoad(void);
void Hmi_SetTask(void);

#endif


