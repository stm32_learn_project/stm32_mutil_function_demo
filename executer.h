#ifndef _EXECUTER_H_
#define _EXECUTER_H_
#endif

#include "stm32f10x.h"                  // Device header
void TIM2_IRQHandler(void);
void EXTI0_IRQHandler(void);

extern uint64_t timer_count;

