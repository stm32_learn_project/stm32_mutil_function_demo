#include "stm32f10x_tim.h"              // Keil::Device:StdPeriph Drivers:TIM
#include "stm32f10x.h"                  // Device header
#include "base_configure.h"
#include "delay.h"
#include "display.h"

void Delay_Timer_Config(void)
{
	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStruct;
	
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE );
	
//	TIM_ETRClockMode2Config(TIM2, TIM_ExtTRGPSC_OFF, TIM_ExtTRGPolarity_NonInverted, 0x0F);

	
	
	TIM_TimeBaseInitStruct.TIM_Period = (1000-1);
	TIM_TimeBaseInitStruct.TIM_Prescaler = (72-1);  //中断一次 1ms
	
//	TIM_TimeBaseInitStruct.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseInitStruct.TIM_CounterMode = TIM_CounterMode_Up;  //默认向上
//	TIM_TimeBaseInitStruct.TIM_RepetitionCounter = 0x01;
	TIM_TimeBaseInit(TIM2,&TIM_TimeBaseInitStruct);

	//配置中断
	TIM_ClearFlag(TIM2,TIM_FLAG_Update);
	TIM_ITConfig(TIM2,TIM_IT_Update,ENABLE);

	//使能计数
	TIM_Cmd(TIM2,ENABLE);
}

void OS_Cell_Run_Timer_Config(void)
{
	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStruct;
	
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE );
	
//	TIM_ETRClockMode2Config(TIM2, TIM_ExtTRGPSC_OFF, TIM_ExtTRGPolarity_NonInverted, 0x0F);

	
	
	TIM_TimeBaseInitStruct.TIM_Period = (1000-1);
	TIM_TimeBaseInitStruct.TIM_Prescaler = (72-1);  //中断一次 1ms
	
//	TIM_TimeBaseInitStruct.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseInitStruct.TIM_CounterMode = TIM_CounterMode_Up;  //默认向上
//	TIM_TimeBaseInitStruct.TIM_RepetitionCounter = 0x01;
	TIM_TimeBaseInit(TIM3,&TIM_TimeBaseInitStruct);

	//配置中断
	TIM_ClearFlag(TIM3,TIM_FLAG_Update);
	TIM_ITConfig(TIM3,TIM_IT_Update,ENABLE);

	//使能计数
	TIM_Cmd(TIM3,ENABLE);
}

void KEY_GPIO_Init(void)
{
	
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB|RCC_APB2Periph_GPIOA, ENABLE);
	
	GPIO_InitTypeDef GPIO_InitStructure;
 	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12| GPIO_Pin_13| GPIO_Pin_14| GPIO_Pin_15|GPIO_Pin_0;
 	GPIO_Init(GPIOB, &GPIO_InitStructure);
	
	GPIO_ResetBits(GPIOB,GPIO_Pin_12);
	GPIO_ResetBits(GPIOB,GPIO_Pin_13);
	GPIO_ResetBits(GPIOB,GPIO_Pin_14);
	GPIO_ResetBits(GPIOB,GPIO_Pin_15);
	GPIO_ResetBits(GPIOA,GPIO_Pin_0);
	
}


void LED_GPIO_Init(void)
{	
	//1.打开控制GPIOC的时钟(APB2)
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	
	//2.配置结构体	
	GPIO_InitTypeDef led_init;
	led_init.GPIO_Pin   = GPIO_Pin_2;      //GPIOB13引脚
	led_init.GPIO_Mode  = GPIO_Mode_Out_PP; //推挽输出	
	led_init.GPIO_Speed = GPIO_Speed_10MHz; //10MHz
	
	//3.对成员进行初始化
	GPIO_Init(GPIOB, &led_init);
	
	//初始化拉低电压
	GPIO_ResetBits(GPIOB, GPIO_Pin_2);
}

void OLED_GPIO_Init(void)
{

	
	/*在初始化前，加入适量延时，待OLED供电稳定*/

	
	/*将SCL和SDA引脚初始化为开漏模式*/
   RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	
	GPIO_InitTypeDef GPIO_InitStructure;
 	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_OD;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8;
 	GPIO_Init(GPIOB, &GPIO_InitStructure);
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
 	GPIO_Init(GPIOB, &GPIO_InitStructure);
	
	
}

void KEY_EXTI_Init(void)
{

    EXTI_InitTypeDef EXTI_InitStruct;
   
    /* exti 开启afio时钟 */
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);

//    EXTI_DeInit();
    /* exti中断IO口配置 */
    GPIO_EXTILineConfig(GPIO_PortSourceGPIOA, GPIO_PinSource0);
    /* exti配置 */
    EXTI_InitStruct.EXTI_Line = EXTI_Line0;
    EXTI_InitStruct.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_InitStruct.EXTI_Trigger = EXTI_Trigger_Falling;
    EXTI_InitStruct.EXTI_LineCmd = ENABLE;
    EXTI_Init(&EXTI_InitStruct);

    EXTI_ClearITPendingBit(EXTI_Line0);
	
		
}

void Delay_Timer_NVIC_Config(void)
{
	NVIC_InitTypeDef NVIC_InitStruct;
	 /* exti中断nvic配置 */
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_1);
	
	NVIC_InitStruct.NVIC_IRQChannel = TIM2_IRQn;
	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 1; //只有一个中断，随意配
	NVIC_InitStruct.NVIC_IRQChannelSubPriority = 1;
	NVIC_Init(&NVIC_InitStruct);
}
void OS_Cell_Timer_NVIC_Config(void)
{
	NVIC_InitTypeDef NVIC_InitStruct;
	 /* exti中断nvic配置 */
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_1);
	
	NVIC_InitStruct.NVIC_IRQChannel = TIM3_IRQn;
	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 1; //只有一个中断，随意配
	NVIC_InitStruct.NVIC_IRQChannelSubPriority = 1;
	NVIC_Init(&NVIC_InitStruct);
}



void Init_configure(void)
	{
		OLED_GPIO_Init();
		LED_GPIO_Init();
		KEY_GPIO_Init();
		OS_Cell_Run_Timer_Config();
		Delay_Timer_Config();
		KEY_EXTI_Init();
		Delay_Timer_NVIC_Config();
		OS_Cell_Timer_NVIC_Config();
		delay_ms(200);
		/*OLED初始化*/
		OLED_Init();
	}



