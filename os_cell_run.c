#include "stm32f10x.h"                  // Device header
#include "os_cell_run.h"
#include "stdlib.h"


#include "led_demo.h"


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int compareTasks(const void *a, const void *b) {
    struct TaskStruct *task1 = (struct TaskStruct *)a;
    struct TaskStruct *task2 = (struct TaskStruct *)b;
    return (task1->TaskID - task2->TaskID);
}
void sort(struct TaskStruct *TaskST  )//a为数组地址，l为数组长度。
{
		int size = sizeof(TaskST) ; // 计算数组大小
		qsort(TaskST, size, sizeof(struct TaskStruct), compareTasks);
}

/************任务初始化，每一个结构体都代表一个任务，添加任务和删减任务都在这里完成************/
struct TaskStruct TaskST[]=
{
 //任务优先级  计时   多少ms调用一次    任务运行标志位    任务函数指针
	{3, 		0,       5000,              0,           demo1},
	{1, 		0,       1000,              0,		   		 demo2},
	{2,			0,       3000,              0,           demo3},
};

/*******************************搭建时间片轮询机制代码框架********************************/

//记录任务数量
u8 TaskCount=	sizeof(TaskST)/sizeof(TaskST[0]);	

//放在“TIM2_IRQHandler”中断执行，用于任务计时
void OS_IT_RUN(void)
{
	u8 i;
	for(i=0;i<TaskCount;i++)//遍历所有循环
	{	
		if(!TaskST[i].TaskStatus)
		{		
			if(++TaskST[i].TaskTickNow >= TaskST[i].TaskTickMax)//计时，并判断是否到达定时时间
			{	
				TaskST[i].TaskTickNow = 0;
				TaskST[i].TaskStatus = 1;
			}
		}
	}
}

//放在main函数中执行，自带死循环，用于执行挂起的任务
void PeachOSRun(void)
{
	sort(TaskST);
	u8 j=0;
	while(1)
	{
		if(TaskST[j].TaskStatus)//判断一个任务是否被挂起
		{		
			TaskST[j].FC();				//执行该任务函数
			TaskST[j].TaskStatus=0;		//任务的挂起
		
		}
		if(++j>=TaskCount)//相当于不断循环遍历所有的任务
			j=0;		
	}
}


/*************************计划表中的任务*****************************/
void demo1(void)
{
		demo(1);
	//printf("Led \n");//调试用，需要自己添加uart外设后再使用这段代码
}
	
void demo2(void)
{
	demo(2);
	//printf("UARTFC \n");//调试用
}
	
void demo3(void)
{
	demo(3);
	//printf("UserTestMode \n");//调试用
}

