#ifndef _HMI_MUSIC_H_
#define _HMI_MUSIC_H_

#include "hmi_common.h"


extern const MenuImage_t sgc_MusicImage;

void Hmi_MusicEnter(void);
void Hmi_MusicExit(void);
void Hmi_MusicLoad(void);
void Hmi_MusicTask(void);

#endif


