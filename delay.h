#ifndef _DELAY_H_
#define _DELAY_H_
#endif

#include "stm32f10x.h"                  // Device header
#include "stm32f10x_tim.h"              // Keil::Device:StdPeriph Drivers:TIM

void delay_ms(u32 time);
void delay_us(u32 time);
void BaseTimer_Config(void);




#define Delay_ms delay_ms
#define Delay_us delay_us
