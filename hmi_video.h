#ifndef _HMI_VIDEO_H_
#define _HMI_VIDEO_H_

#include "hmi_common.h"

extern const MenuImage_t sgc_VideoImage;

void Hmi_VideoLoad(void);
void Hmi_VideoExit(void);
void Hmi_VideoTask(void);

#endif 
#define printf Hmi_VideoTask
