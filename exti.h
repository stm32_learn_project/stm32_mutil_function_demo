#ifndef _EXTI_H_
#define _EXTI_H_
#endif

#include "stm32f10x.h"                  // Device header
void KEY_EXTI_Init(void);
void EXTI_ITConfig(u32 EXTI_LINEn, FunctionalState state);
void KEY_EXTI_Config(u32 EXTI_LINEn, EXTITrigger_TypeDef EXTITrigger_x, FunctionalState state);
void BASETimer_NVIC_Config(void);


//#define KEY0_State

/* 按键中断模式 ----------- */
enum KEY0_State {
    KEY0_DOWN = 1,  /* 当KEY0按下时，IO口状态为低电平 */
    KEY0_UP = 0     /* 当KEY0未按下时，IO口状态为高电平 */
}extern  KEY0_State;



//#define KEY0_EXTI_IRQn                  EXTI0_IRQn
//#define KEY0_EXTIn_IRQHandler           EXTI0_IRQHandler
//#define KEY0_EXTI_Line                  EXTI_Line0

///* key0 define */
//#define KEY0                            PEin(0)
//#define KEY0_GPIO_Pin                   GPIO_Pin_0
//#define KEY0_GPIO_Port                  GPIOA
//#define KEY0_RCC_APB2Periph_CLK         RCC_APB2Periph_GPIOA

