#ifndef _OS_CELL_RUN_H_
#define _OS_CELL_RUN_H_
#endif


#include "stm32f10x.h"

//框架运行所需的函数声明
void PeachOSRun(void);
void OS_IT_RUN(void);

/*
用与存储一个任务运行的数据
*/
struct TaskStruct
{	u16 TaskID; //任务优先级
	u16	TaskTickNow;//用于计时
	u16	TaskTickMax;//设置计时时间
	u8	TaskStatus;//任务运行标志位
	void (*FC)();//任务函数指针
};

extern struct TaskStruct TaskST[];	//声明为结构体数据，那样多个任务时方便管理

//用于示例的任务函数声明
void demo1(void);
void demo2(void);
void demo3(void);




