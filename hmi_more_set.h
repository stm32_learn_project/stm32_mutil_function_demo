#ifndef _HMI_MORE_SET_H_
#define _HMI_MORE_SET_H_

#include "hmi_common.h"


void Hmi_MoreSetEnter(void);
void Hmi_MoreSetExit(void);
void Hmi_MoreSetLoad(void);
void Hmi_MoreSetTask(void);

#endif


