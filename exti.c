#include "stm32f10x_exti.h"             // Keil::Device:StdPeriph Drivers:EXTI
#include "stm32f10x.h"
#include "led.h"
#include "delay.h"
#include "key.h"
#include "stdio.h"
#include "led_executer.h"
#include "exti.h"
#include "os_cell_run.h"


/**
 * @name: EXTI_ITConfig
 * @description: 开关exti中断
 * @param {u32} EXTI_LINEn exti中断线
 * @param {FunctionalState} state exti中断状态
 * @return {*}
 */
 void EXTI_ITConfig(u32 EXTI_LINEn, FunctionalState state)
{
    if(state)
        EXTI->IMR |= EXTI_LINEn;
    else
        EXTI->IMR &= ~EXTI_LINEn;

}

/**
 * @name: KEY_EXTI_Config
 * @description: exti结构体配置
 * @param {u32} EXTI_LINEn  exti线
 * @param {EXTITrigger_TypeDef} EXTITrigger_x   exti触发模式
 * @param {FunctionalState} state  exti中断使能
 * @return {*}
 */
 void KEY_EXTI_Config(u32 EXTI_LINEn, EXTITrigger_TypeDef EXTITrigger_x, FunctionalState state)
{
    EXTI_InitTypeDef EXTI_InitStruct;

    EXTI_InitStruct.EXTI_Line = EXTI_LINEn;
    EXTI_InitStruct.EXTI_Trigger = EXTITrigger_x;
    EXTI_InitStruct.EXTI_LineCmd = state;
    EXTI_InitStruct.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_Init(&EXTI_InitStruct);
}




/**
 * @name: KEY0_EXTIn_IRQHandler
 * @description: exti中断处理函数
 * @param {*}
 * @return {*}
 */
  uint8_t is_rising = 0;
typedef enum LedStuts {
    stuts_1 = 1, 
    stuts_2 = 2,
		stuts_3 = 3   
} LedStuts;
static u8 stuts=1;
void EXTI0_IRQHandler(void)
{
    /* 局部静态变量，用于触发沿判断 */
   
		if (stuts==3){
			stuts++;
		}
    switch (stuts)
    {
			case 1:
        {/* Falling Trigger */
        EXTI_ITConfig(EXTI_Line0, DISABLE); /* 关EXTI线中断 */
        delay_ms(10);   /* 去抖 */
        if(GPIO_ReadInputDataBit(GPIOA,GPIO_Pin_0) == KEY0_DOWN)   /* 是否按下 */
        {
            /* 配置下次上升沿触发 */
            KEY_EXTI_Config(EXTI_Line0, EXTI_Trigger_Rising, ENABLE);
            is_rising = 1;
						led_bright_to_weak();
//            fprintf("worker1 %s",is_rising);//工作函数
        }
        else
        {
            EXTI_ITConfig(EXTI_Line0, ENABLE);
        }
			}
		
			case 2:
			{
        /* Rising Trigger */
        EXTI_ITConfig(EXTI_Line0, DISABLE); /* 关EXTI线中断 */
        delay_ms(10); /* 去抖 */
        if(GPIO_ReadInputDataBit(GPIOA,GPIO_Pin_0) == KEY0_UP) /* 是否松开 */
        {
            /* 配置下次下降沿触发 */
            KEY_EXTI_Config(EXTI_Line0, EXTI_Trigger_Falling, ENABLE);
            is_rising = 0;

           led_breathing_lamp();
//            sprintf("worker1 %s",is_rising);//工作函数
        }
        else
        {
            EXTI_ITConfig(EXTI_Line0, ENABLE);
        }
			}
		};
    EXTI_ClearITPendingBit(EXTI_Line0);
}


uint64_t timer_count = 0;
void TIM2_IRQHandler(void)
{
	if(TIM_GetITStatus(TIM2,TIM_IT_Update)==1)
	{
		timer_count++;
		TIM_ClearITPendingBit(TIM2,TIM_IT_Update);
	}
	
}



void TIM3_IRQHandler(void)
{
	if(TIM_GetITStatus(TIM3,TIM_IT_Update)==1)
	{
		OS_IT_RUN();
		TIM_ClearITPendingBit(TIM3,TIM_IT_Update);
	}
	
}


