#ifndef _HMI_CAMERA_H_
#define _HMI_CAMERA_H_

#include "hmi_common.h"

extern const MenuImage_t sgc_CameraImage;

void Hmi_CameraEnter(void);
void Hmi_CameraExit(void);
void Hmi_CameraLoad(void);
void Hmi_CameraTask(void);

#endif


