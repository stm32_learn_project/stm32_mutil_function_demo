#include "stm32f10x.h"                  // Device header
#include "stm32f10x_gpio.h"             // Keil::Device:StdPeriph Drivers:GPIO
#include "GPIO_STM32F10x.h"             // Keil::Device:GPIO
#include "RTE_Device.h"                 // Keil::Device:Startup
#include "misc.h"                       // Keil::Device:StdPeriph Drivers:Framework
#include "RTE_Components.h"             // Component selection
#include "stm32f10x_dbgmcu.h"           // Keil::Device:StdPeriph Drivers:DBGMCU
#include "stm32f10x_rcc.h"              // Keil::Device:StdPeriph Drivers:RCC
#include "stm32f10x_i2c.h"              // Keil::Device:StdPeriph Drivers:I2C


#include "delay.h"
#include "display.h"
#include "exti.h"
#include "key.h"
#include "executer.h"
#include "led_executer.h"
#include "led.h"
#include "base_configure.h"
#include "os_cell_run.h"



/**
  * 坐标轴定义：
  * 左上角为(0, 0)点
  * 横向向右为X轴，取值范围：0~127
  * 纵向向下为Y轴，取值范围：0~63
  * 
  *       0             X轴           127 
  *      .------------------------------->
  *    0 |
  *      |
  *      |
  *      |
  *  Y轴 |
  *      |
  *      |
  *      |
  *   63 |
  *      v
  * 
  */

int main(void)
{
	
	Init_configure();

	PeachOSRun();
	
	
}
